﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OOP_QuanLiRutTien
{
    public interface IQueueInBank
    {
        void AddCustomer(Customer customer);
        Customer CallNextCustomer();
        string PrintCustomer();
    }
}