﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_QuanLiRutTien
{
    public class QueueInBank : IQueueInBank
    {
        Customer[] customers;
        int maxQueue;
        int bot = -1;
        int top = -1;
        public QueueInBank(int _maxQueue)
        {
            maxQueue = _maxQueue;
            customers = new Customer[maxQueue];
            top = 0;
        }
        public void AddCustomer(Customer customer)
        {
            bot++;
            customer.Count = bot + 1;
            if (bot > maxQueue)
            {
                throw new Exception("Queue is full");
            }
            customers[bot] = customer;
        }

        public Customer CallNextCustomer()
        {
            var customer = customers[top];
            top++;
            return customer;
        }

        public string PrintCustomer()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bot; i++)
            {
                sb.Append(customers[i].Count + " | "+ customers[i].Name+" | " + customers[i].Money);
            }
            return sb.ToString();
        }
    }
}
