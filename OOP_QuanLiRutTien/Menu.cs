﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_QuanLiRutTien
{
    public class Menu
    {
        public void MainMenu()
        {
            Console.WriteLine("----------ATM----------");
            Console.WriteLine("| 1. Add customer     |");
            Console.WriteLine("| 2. Call customer    |");
            Console.WriteLine("| 3. Print customer   |");
            Console.WriteLine("| 4. Exit             |");
            Console.WriteLine("-----------------------");
        }
    }
}
