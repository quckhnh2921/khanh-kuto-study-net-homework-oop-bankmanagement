﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_QuanLiRutTien
{
    public class Customer
    {
        private int count;
        private string name;
        private decimal money;

        public Customer()
        {
            
        }
        public Customer(int _count, string _name, decimal _money)
        {
            count = _count;
            name = _name;
            money = _money;
        }

        public int Count
        {
            get => count;
            set => count = value;
        }

        public string Name
        {
            get => name;
            set => name = value;
        }

        public decimal Money
        {
            get => money;
            set
            {
                if(money <= 0)
                {
                    throw new Exception("Money is invalid");
                }
                money = value;
            }
        }
    }
}
