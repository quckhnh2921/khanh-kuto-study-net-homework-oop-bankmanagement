﻿using OOP_QuanLiRutTien;

IQueueInBank queue = new QueueInBank(10);
Menu menu = new Menu();
int choice = 0;
do
{
    try
    {
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Customer customer = new Customer();
                Console.Write("Enter name: ");
                customer.Name = Console.ReadLine();
                Console.Write("Enter money to withdraw ");
                customer.Money=decimal.Parse(Console.ReadLine());
                queue.AddCustomer(customer);
                break;
            case 2:
                var nextCustomer = queue.CallNextCustomer();
                Console.WriteLine(nextCustomer.Count + " | " + nextCustomer.Name + " | " + nextCustomer.Money);
                break;
            case 3:
                Console.WriteLine(queue.PrintCustomer());
                break;
            case 4:
                Console.WriteLine("Bye Bye !");
                Environment.Exit(0);
                break;
            default:
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);